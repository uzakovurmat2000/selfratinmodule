﻿using dbquestion.Data;
using dbquestion.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

public class CriterionController : Controller
{
    private readonly QuestionsContext _context;

    public CriterionController(QuestionsContext context)
    {
        _context = context;
    }

    // Действие для отображения списка критериев
    [Authorize(Policy = "RequireAdmin")]
    public IActionResult Index()
    {
        var criteria = _context.Criteria.Include(c => c.Options).ToList();
        // Получение данных о прохождениях теста
        var testCompletion = _context.TestCompletions.FirstOrDefault(entry => entry.Date == DateTime.Now.Date);

        // Передача данных в представление
        ViewBag.DailyCount = testCompletion?.DailyCount ?? 0;
        ViewBag.TotalCount = testCompletion?.TotalCount ?? 0;
        return View(criteria);
    }

    // Действие для отображения формы добавления критерия
    [HttpGet]
    public IActionResult AddCriterion()
    {
        // Получение данных о прохождениях теста
        var testCompletion = _context.TestCompletions.FirstOrDefault(entry => entry.Date == DateTime.Now.Date);

        // Передача данных в представление
        ViewBag.DailyCount = testCompletion?.DailyCount ?? 0;
        ViewBag.TotalCount = testCompletion?.TotalCount ?? 0;
        return View(new Criterion());
    }
    // Действие для обработки добавления критерия
    [HttpPost]
    public IActionResult AddCriterion(Criterion model, List<string> options, List<int?> scores)
    {
        if (ModelState.IsValid)
        {
            // Добавляем критерий
            _context.Criteria.Add(model);
            _context.SaveChanges();

            // Добавляем варианты ответов и связываем их с критерием
            for (int i = 0; i < options.Count; i++)
            {
                var option = new Option
                {
                    Optiontext = options[i],
                    Score = scores[i],
                    Criteriaid = model.Criteriaid  // Устанавливаем связь с критерием
                };

                model.Options.Add(option); // Добавляем вариант ответа в коллекцию критерия
                _context.Options.Add(option); // Добавляем вариант ответа в контекст данных
            }

            // Сохраняем изменения в базе данных
            _context.SaveChanges();

            return RedirectToAction(nameof(Index));
        }

        return View(model);
    }


    // Действие для отображения формы редактирования критерия
    [HttpGet]
    public IActionResult EditCriterion(int? id)
    {
        // Получение данных о прохождениях теста
        var testCompletion = _context.TestCompletions.FirstOrDefault(entry => entry.Date == DateTime.Now.Date);

        // Передача данных в представление
        ViewBag.DailyCount = testCompletion?.DailyCount ?? 0;
        ViewBag.TotalCount = testCompletion?.TotalCount ?? 0;

        var model = new Criterion();

        if (id.HasValue)
        {
            model = _context.Criteria.Include(c => c.Options).FirstOrDefault(c => c.Criteriaid == id);

            if (model == null)
            {
                return NotFound();
            }
        }

        return View(model);
    }


    [HttpPost]
    [ValidateAntiForgeryToken]
 
    public async Task<IActionResult> EditCriterion(int id, Criterion model)
    {
        if (id != model.Criteriaid)
        {
            return NotFound();
        }

        if (ModelState.IsValid)
        {
            try
            {
                var existingCriterion = await _context.Criteria
                    .Include(c => c.Options)
                    .FirstOrDefaultAsync(c => c.Criteriaid == id);

                if (existingCriterion != null)
                {
                    // Обновляем свойства критерия
                    existingCriterion.Criteriatext = model.Criteriatext;

                    // Удаляем существующие варианты ответов
                    _context.Options.RemoveRange(existingCriterion.Options);

                    // Добавляем новые варианты ответов из модели
                    foreach (var optionModel in model.Options)
                    {
                        var option = new Option
                        {
                            Optiontext = optionModel.Optiontext,
                            Score = optionModel.Score
                        };

                        existingCriterion.Options.Add(option);
                    }

                    await _context.SaveChangesAsync();
                }
            }
            catch (DbUpdateConcurrencyException)
            {
                // Обработка исключения
            }
            return RedirectToAction(nameof(Index));
        }

        return View(model);
    }

    // Действие для отображения формы удаления критерия
    [HttpGet]
    public IActionResult DeleteCriterion(int id)
    {
        // Получение данных о прохождениях теста
        var testCompletion = _context.TestCompletions.FirstOrDefault(entry => entry.Date == DateTime.Now.Date);

        // Передача данных в представление
        ViewBag.DailyCount = testCompletion?.DailyCount ?? 0;
        ViewBag.TotalCount = testCompletion?.TotalCount ?? 0;

        var criterion = _context.Criteria.Include(c => c.Options).FirstOrDefault(c => c.Criteriaid == id);

        if (criterion == null)
        {
            return NotFound();
        }

        return View(criterion);
    }

    // Действие для обработки удаления критерия
    [HttpPost, ActionName("DeleteCriterion")]
    public IActionResult ConfirmDeleteCriterion(int id)
    {
        var criterion = _context.Criteria.Include(c => c.Options).FirstOrDefault(c => c.Criteriaid == id);

        if (criterion == null)
        {
            return NotFound();
        }

        // Удаление связанных вариантов ответов
        _context.Options.RemoveRange(criterion.Options);

        // Удаление критерия
        _context.Criteria.Remove(criterion);
        _context.SaveChanges();

        return RedirectToAction(nameof(Index));
    }

    [HttpGet]
    public IActionResult AddOptions(int id)
    {
        var criterion = _context.Criteria.Include(c => c.Options).FirstOrDefault(c => c.Criteriaid == id);

        if (criterion == null)
        {
            return NotFound();
        }

        var viewModel = new Option
        {
            Criteriaid = criterion.Criteriaid
        };

        return View(viewModel);
    }
    public IActionResult AddOptions(Option model)
    {
        if (ModelState.IsValid)
        {
            var criterion = _context.Criteria.Include(c => c.Options).FirstOrDefault(c => c.Criteriaid == model.Criteriaid);

            if (criterion != null)
            {
                foreach (var optionText in model.Optiontext)
                {
                    var option = new Option
                    {
                        Optiontext = model.Optiontext,
                        Score = model.Score,
                        Criteriaid = model.Criteriaid
                    };

                    criterion.Options.Add(option);
                }

                _context.SaveChanges();

                return RedirectToAction(nameof(Index));
            }
        }

        return View(model);
    }
}
