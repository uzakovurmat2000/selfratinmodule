﻿using dbquestion.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using dbquestion.Models; // Замените на ваше пространство имен
using dbquestion.Data;
using Microsoft.AspNetCore.Authorization;

public class QuestionController : Controller
{
    private readonly QuestionsContext _context;

    public QuestionController(QuestionsContext context)
    {
        _context = context;
    }
    [Authorize(Policy = "RequireAdmin")]
    public IActionResult Index()
    {
        List<Question> questions = _context.Questions.ToList();
        // Получение данных о прохождениях теста
        var testCompletion = _context.TestCompletions.FirstOrDefault(entry => entry.Date == DateTime.Now.Date);

        // Передача данных в представление
        ViewBag.DailyCount = testCompletion?.DailyCount ?? 0;
        ViewBag.TotalCount = testCompletion?.TotalCount ?? 0;
        return View(questions);
    }

    [HttpGet]
    public IActionResult Create()
    {
        // Получение данных о прохождениях теста
        var testCompletion = _context.TestCompletions.FirstOrDefault(entry => entry.Date == DateTime.Now.Date);

        // Передача данных в представление
        ViewBag.DailyCount = testCompletion?.DailyCount ?? 0;
        ViewBag.TotalCount = testCompletion?.TotalCount ?? 0;

        return View();
    }

    [HttpPost]
    public IActionResult Create(Question question)
    {
        if (ModelState.IsValid)
        {
            _context.Questions.Add(question);
            _context.SaveChanges();
            return RedirectToAction(nameof(Index));
        }
        

        return View(question);
    }

    [HttpGet]
    public IActionResult Edit(int id)
    {
        // Получение данных о прохождениях теста
        var testCompletion = _context.TestCompletions.FirstOrDefault(entry => entry.Date == DateTime.Now.Date);

        // Передача данных в представление
        ViewBag.DailyCount = testCompletion?.DailyCount ?? 0;
        ViewBag.TotalCount = testCompletion?.TotalCount ?? 0;

        var question = _context.Questions.Find(id);

        if (question == null)
        {
            return NotFound();
        }

        return View(question);
    }

    [HttpPost]
    public IActionResult Edit(Question question)
    {
        if (ModelState.IsValid)
        {
            _context.Entry(question).State = EntityState.Modified;
            _context.SaveChanges();
            return RedirectToAction(nameof(Index));
        }

        return View(question);
    }

    [HttpGet]
    public IActionResult Delete(int id)
    {
        // Получение данных о прохождениях теста
        var testCompletion = _context.TestCompletions.FirstOrDefault(entry => entry.Date == DateTime.Now.Date);

        // Передача данных в представление
        ViewBag.DailyCount = testCompletion?.DailyCount ?? 0;
        ViewBag.TotalCount = testCompletion?.TotalCount ?? 0;

        var question = _context.Questions.Find(id);

        if (question == null)
        {
            return NotFound();
        }

        return View(question);
    }

    [HttpPost, ActionName("Delete")]
    public IActionResult DeleteConfirmed(int id)
    {
        var question = _context.Questions.Find(id);

        if (question != null)
        {
            _context.Questions.Remove(question);
            _context.SaveChanges();
        }

        return RedirectToAction(nameof(Index));
    }
}
