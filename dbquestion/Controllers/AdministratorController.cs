﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using dbquestion.Models;
using dbquestion.Data;
using Microsoft.AspNetCore.Authorization;

namespace dbquestion.Controllers
{
    [Authorize]
    public class AdministratorController : Controller
    {
        private readonly QuestionsContext _context;

        public AdministratorController(QuestionsContext context)
        {
            _context = context;
        }

        // GET: Administrator
        public async Task<IActionResult> Index()
        {
            var administrators = await _context.Administrators.ToListAsync();
            // Получение данных о прохождениях теста
            var testCompletion = _context.TestCompletions.FirstOrDefault(entry => entry.Date == DateTime.Now.Date);

            // Передача данных в представление
            ViewBag.DailyCount = testCompletion?.DailyCount ?? 0;
            ViewBag.TotalCount = testCompletion?.TotalCount ?? 0;
            return View(administrators);
        }
  
        public async Task<IActionResult> AdminPanel()
        {
            // Получение данных о прохождениях теста
            var testCompletion = _context.TestCompletions.FirstOrDefault(entry => entry.Date == DateTime.Now.Date);

            // Передача данных в представление
            ViewBag.DailyCount = testCompletion?.DailyCount ?? 0;
            ViewBag.TotalCount = testCompletion?.TotalCount ?? 0;
            return View();
        }

        // GET: Administrator/Create
        public IActionResult Create()
        {
            // Получение данных о прохождениях теста
            var testCompletion = _context.TestCompletions.FirstOrDefault(entry => entry.Date == DateTime.Now.Date);

            // Передача данных в представление
            ViewBag.DailyCount = testCompletion?.DailyCount ?? 0;
            ViewBag.TotalCount = testCompletion?.TotalCount ?? 0;
            return View();
        }

        // POST: Administrator/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Administratorid,Username,Passwordhash")] Administrator administrator)
        {
            if (ModelState.IsValid)
            {
                _context.Add(administrator);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(administrator);
        }

        // GET: Administrator/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            // Получение данных о прохождениях теста
            var testCompletion = _context.TestCompletions.FirstOrDefault(entry => entry.Date == DateTime.Now.Date);

            // Передача данных в представление
            ViewBag.DailyCount = testCompletion?.DailyCount ?? 0;
            ViewBag.TotalCount = testCompletion?.TotalCount ?? 0;
            if (id == null)
            {
                return NotFound();
            }

            Administrator administrator = await _context.Administrators.FindAsync(id);

            if (administrator == null)
            {
                return NotFound();
            }
            return View(administrator);
        }

        // POST: Administrator/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Administratorid,Username,Passwordhash")] Administrator administrator)
        {
            if (id != administrator.Administratorid || administrator == null)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(administrator);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AdministratorExists(administrator.Administratorid))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(administrator);
        }

        // GET: Administrator/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            // Получение данных о прохождениях теста
            var testCompletion = _context.TestCompletions.FirstOrDefault(entry => entry.Date == DateTime.Now.Date);

            // Передача данных в представление
            ViewBag.DailyCount = testCompletion?.DailyCount ?? 0;
            ViewBag.TotalCount = testCompletion?.TotalCount ?? 0;
            if (id == null)
            {
                return NotFound();
            }

            Administrator administrator = await _context.Administrators
                .FirstOrDefaultAsync(m => m.Administratorid == id);

            if (administrator == null)
            {
                return NotFound();
            }

            return View(administrator);
        }

        // POST: Administrator/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            Administrator administrator = await _context.Administrators.FindAsync(id);
            _context.Administrators.Remove(administrator);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool AdministratorExists(int id)
        {
            return _context.Administrators.Any(e => e.Administratorid == id);
        }
    }
}
