﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using dbquestion.Data;
using dbquestion.Models;
using System.Collections.Generic;
using System.Linq;

public class TestController : Controller
{
    private readonly QuestionsContext _context;

    public TestController(QuestionsContext context)
    {
        _context = context;
    }

    public IActionResult Index()
    {
        var questions = _context.Questions.ToList();
        var criteria = _context.Criteria.Include(c => c.Options).ToList();

        var viewModel = new Tuple<List<Question>, List<Criterion>>(questions, criteria);

        // Получение данных о прохождениях теста
        var testCompletion = _context.TestCompletions.FirstOrDefault(entry => entry.Date == DateTime.Now.Date);

        // Передача данных в представление
        ViewBag.DailyCount = testCompletion?.DailyCount ?? 0;
        ViewBag.TotalCount = testCompletion?.TotalCount ?? 0;


        return View("Test", viewModel);
    }

    [HttpPost]
    public IActionResult SubmitAnswers(Dictionary<string, string> selectedOptionIds)
    {
        var questions = _context.Questions.ToList();
        var criteria = _context.Criteria.Include(c => c.Options).ToList();

        var totalScores = new List<double>(); // Изменено: используем double
        var totalSelectedOptions = 0;

        foreach (var question in questions)
        {
            var questionId = question.Questionid.ToString();

            // Суммируем баллы для каждого вопроса
            var questionScore = 0.0; // Изменено: используем double

            // Считаем количество выбранных ответов для каждого вопроса
            var selectedOptionsCount = 0;

            // Проверяем наличие выбранного ответа для каждого критерия в вопросе
            foreach (var criterion in criteria)
            {
                if (selectedOptionIds.TryGetValue($"{questionId}_{criterion.Criteriaid}", out var selectedOptionIdString) && int.TryParse(selectedOptionIdString, out var selectedOptionId))
                {
                    questionScore += CalculateCriterionScore(criterion, selectedOptionId);
                    selectedOptionsCount++;
                }
            }

            totalScores.Add(questionScore);
            totalSelectedOptions += selectedOptionsCount;
        }
        
        
        var averageScore = totalSelectedOptions > 0 ? totalScores.Sum() / totalSelectedOptions : 0.0; // Изменено: используем double

        ViewBag.AverageScore = averageScore;

        // Определение уровня
        string level;
        if (averageScore <= 1)
        {
            level = "Синий";
        }
        else if (averageScore <= 2)
        {
            level = "Зелёный";
        }
        else if (averageScore <= 6)
        {
            level = "Желтый";
        }
        else
        {
            level = "Красный";
        }

        ViewBag.Level = level;

        // Получение текущей даты

        DateTime currentDate = DateTime.Now.Date;

        // Получение последней записи в таблице TestCompletion
        var lastEntry = _context.TestCompletions.OrderByDescending(entry => entry.Date).FirstOrDefault();

        if (lastEntry == null)
        {
            // Если нет записей в таблице, создаем новую с начальными значениями
            lastEntry = new TestCompletion
            {
                Date = currentDate,
                DailyCount = 0,
                TotalCount = 0
            };

            _context.TestCompletions.Add(lastEntry);
        }

        if (lastEntry.Date == currentDate)
        {
            // Если дата последней записи совпадает с текущей датой, увеличиваем значения
            lastEntry.DailyCount += 1;
        }
        else
        {
            // Если дата не совпадает, обновляем данные для новой даты
            lastEntry.Date = currentDate;
            lastEntry.DailyCount = 1;
        }

        // В любом случае увеличиваем TotalCount
        lastEntry.TotalCount += 1;

        _context.TestCompletions.Update(lastEntry);
        _context.SaveChanges();



        return PartialView("TestResult");
    }

    private double CalculateCriterionScore(Criterion criterion, int selectedOptionId)
    {
        // Используем LINQ для суммирования баллов по выбранному варианту ответа в критерии
        return criterion.Options
            .Where(option => option.Optionid == selectedOptionId)
            .Sum(option => option.Score ?? 0);
    }
   
}
