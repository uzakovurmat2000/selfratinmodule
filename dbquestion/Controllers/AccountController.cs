﻿// Controllers/AccountController.cs

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication;
using System.Security.Claims;
using System.Threading.Tasks;
using dbquestion.Models;
using Microsoft.EntityFrameworkCore;
using dbquestion.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.Cookies;

public class AccountController : Controller
{
    private readonly QuestionsContext _context;

    public AccountController(QuestionsContext context)
    {
        _context = context;
    }
    [AllowAnonymous]
    [HttpGet]
    public IActionResult Login()
    {
        return View();
    }

    [HttpPost]
    public async Task<IActionResult> Login(Administrator model)
    {
        if (!ModelState.IsValid)
        {
            return View(model);
        }

        if (IsValidUser(model.Username, model.Passwordhash))
        {
            var claims = new List<Claim>
        {
            new Claim(ClaimTypes.Name, model.Username),
            new Claim(ClaimTypes.Role, "Administrator")
        };

            var claimsIdentity = new ClaimsIdentity(claims, "login");

            var authProperties = new AuthenticationProperties
            {
                AllowRefresh = true,
                ExpiresUtc = DateTimeOffset.Now.AddMinutes(10),
                IsPersistent = true
            };

            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(claimsIdentity), authProperties);

            return RedirectToAction("AdminPanel", "Administrator");
        }

        ModelState.AddModelError("", "Неверный логин или пароль!");
        return View(model);
    }

    public async Task<IActionResult> Logout()
    {
        await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
        return RedirectToAction("Login");
    }

    private bool IsValidUser(string username, string password)
    {
        // Внимание! Это небезопасный код. Рекомендуется использовать ASP.NET Identity.
        var user = _context.Administrators.FirstOrDefault(u => u.Username == username && u.Passwordhash == password);

        return user != null;
    }
}
