﻿using dbquestion.Data;
using dbquestion.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics;

namespace dbquestion.Controllers
{
    public class HomeController : Controller   
    {
        private readonly QuestionsContext _context;
        private readonly ILogger<HomeController> _logger;  // Добавлено поле для логгера

        public HomeController(QuestionsContext context, ILogger<HomeController> logger)  // Объединение конструкторов
        {
            _context = context;
            _logger = logger;
        }

        public IActionResult Index()
        {
            // Получение данных о прохождениях теста
            var testCompletion = _context.TestCompletions.FirstOrDefault(entry => entry.Date == DateTime.Now.Date);

            // Передача данных в представление
            ViewBag.DailyCount = testCompletion?.DailyCount ?? 0;
            ViewBag.TotalCount = testCompletion?.TotalCount ?? 0;
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}