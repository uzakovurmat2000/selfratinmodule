﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using dbquestion.Models;

namespace dbquestion.Data;

public partial class QuestionsContext : DbContext
{
    public QuestionsContext()
    {
    }

    public QuestionsContext(DbContextOptions<QuestionsContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Administrator> Administrators { get; set; }

    public virtual DbSet<Criterion> Criteria { get; set; }

    public virtual DbSet<Option> Options { get; set; }

    public virtual DbSet<Question> Questions { get; set; }

    public virtual DbSet<Result> Results { get; set; }

    public virtual DbSet<TestCompletion> TestCompletions { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseNpgsql("Host=localhost;Database=db_quest;Username=postgres;");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Administrator>(entity =>
        {
            entity.HasKey(e => e.Administratorid).HasName("administrators_pkey");

            entity.ToTable("administrators");

            entity.Property(e => e.Administratorid).HasColumnName("administratorid");
            entity.Property(e => e.Passwordhash)
                .HasMaxLength(255)
                .HasColumnName("passwordhash");
            entity.Property(e => e.Username)
                .HasMaxLength(255)
                .HasColumnName("username");
        });

        modelBuilder.Entity<Criterion>(entity =>
        {
            entity.HasKey(e => e.Criteriaid).HasName("criteria_pkey");

            entity.ToTable("criteria");

            entity.Property(e => e.Criteriaid).HasColumnName("criteriaid");
            entity.Property(e => e.Criteriatext).HasColumnName("criteriatext");
        });

        modelBuilder.Entity<Option>(entity =>
        {
            entity.HasKey(e => e.Optionid).HasName("options_pkey");

            entity.ToTable("options");

            entity.Property(e => e.Optionid).HasColumnName("optionid");
            entity.Property(e => e.Criteriaid).HasColumnName("criteriaid");
            entity.Property(e => e.Optiontext).HasColumnName("optiontext");
            entity.Property(e => e.Score).HasColumnName("score");

            entity.HasOne(d => d.Criteria).WithMany(p => p.Options)
                .HasForeignKey(d => d.Criteriaid)
                .HasConstraintName("options_criteriaid_fkey");
        });

        modelBuilder.Entity<Question>(entity =>
        {
            entity.HasKey(e => e.Questionid).HasName("questions_pkey");

            entity.ToTable("questions");

            entity.Property(e => e.Questionid).HasColumnName("questionid");
            entity.Property(e => e.Questiontext).HasColumnName("questiontext");
        });

        modelBuilder.Entity<Result>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("results_pkey");

            entity.ToTable("results");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Level)
                .HasColumnType("character varying")
                .HasColumnName("level");
            entity.Property(e => e.Score).HasColumnName("score");
            entity.Property(e => e.Username)
                .HasMaxLength(255)
                .HasColumnName("username");
        });

        modelBuilder.Entity<TestCompletion>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("TestCompletion_pkey");

            entity.ToTable("TestCompletion");

            entity.Property(e => e.Id).ValueGeneratedNever();
            entity.Property(e => e.DailyCount).HasColumnName("DailyCount ");
            entity.Property(e => e.Date).HasColumnType("timestamp without time zone");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
