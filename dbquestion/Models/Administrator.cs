﻿using System;
using System.Collections.Generic;

namespace dbquestion.Models;

public partial class Administrator
{
    public int Administratorid { get; set; }

    public string Username { get; set; } = null!;

    public string Passwordhash { get; set; } = null!;
}
