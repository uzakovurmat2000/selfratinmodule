﻿using System;
using System.Collections.Generic;

namespace dbquestion.Models;

public partial class TestCompletion
{
    public int Id { get; set; }

    public int DailyCount { get; set; }

    public int TotalCount { get; set; }

    public DateTime Date { get; set; }
}
