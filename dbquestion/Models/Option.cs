﻿using System;
using System.Collections.Generic;

namespace dbquestion.Models;

public partial class Option
{
    public int Optionid { get; set; }

    public int? Criteriaid { get; set; }

    public string Optiontext { get; set; } = null!;

    public int? Score { get; set; }

    public virtual Criterion? Criteria { get; set; }
}
