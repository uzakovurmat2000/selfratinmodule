﻿using System;
using System.Collections.Generic;

namespace dbquestion.Models;

public partial class Result
{
    public int Id { get; set; }

    public string? Level { get; set; }

    public int? Score { get; set; }

    public string? Username { get; set; }
}
