﻿using System;
using System.Collections.Generic;

namespace dbquestion.Models;

public partial class Criterion
{
    public int Criteriaid { get; set; }

    public string Criteriatext { get; set; } = null!;

    public virtual ICollection<Option> Options { get; set; } = new List<Option>();
}
